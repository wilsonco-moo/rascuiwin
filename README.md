This is part of the [rascUI project](https://gitlab.com/wilsonco-moo/rascui).

# rascUIwin

This project is an implementation of the rascUI platform system, using the
Windows API. A platform implementation provides all of the platform-specific
stuff, in order for a rascUI based graphical application to run within a
particular platform (in this case desktop Windows). See
[rascUIxcb](https://gitlab.com/wilsonco-moo/rascuixcb) for a
platform implementation for use with the X Window System in Linux.

The [rascUItheme](https://gitlab.com/wilsonco-moo/rascuitheme) project contains
some UI themes for use with rascUIwin, in
[src/rascUItheme/themes/win](https://gitlab.com/wilsonco-moo/rascuitheme/-/tree/master/src/rascUItheme/themes/win).

For information about how to use rascUI (with or without a platform
implementation), see the
[rascUI project](https://gitlab.com/wilsonco-moo/rascui). For an example program
which uses rascUI to provide a cross-platform user interface, see the
[map naming program](https://gitlab.com/wilsonco-moo/mapnamer).

# Screenshots

Here is an example of the rascUI demo program, running using the rascUIwin
platform system, using the scalable theme (from rascUItheme), within Windows 10.

![Image](development/screenshots/windows10.png)

Here is an example of the same program, running within Wine on Linux.

![Image](development/screenshots/wine.png)

Here is an example of the
[map naming program](https://gitlab.com/wilsonco-moo/mapnamer), running using
the rascUIwin platform system, using the scalable theme (from rascUItheme),
within Windows 10.

![Image](development/screenshots/mapnamer.png)
