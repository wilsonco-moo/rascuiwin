/*
 * WinThemeBase.h
 *
 *  Created on: 18 May 2020
 *      Author: wilson
 */

#ifndef RASCUIWIN_THEME_WINTHEMEBASE_H_
#define RASCUIWIN_THEME_WINTHEMEBASE_H_

#include <rascUI/util/Rectangle.h>
#include <rascUI/base/Theme.h>
#include <windef.h>

namespace rascUIwin {
    class Context;
    class Window;

    /**
     * This is a base class for Themes in rascUIwin. It automates basic operations
     * like keeping track of the current Context and Window, and doing painting.
     *
     * This does not (really?) work as a theme on its own. This is intended to either be used
     * as a base class for a theme, or be combined with another theme using a ThemeGroup.
     */
    class WinThemeBase : public rascUI::Theme {
    private:
        // The current context that we are using.
        // When no context is currently in use (outside init - destroy) this is
        // set to NULL.
        Context * context;
        
        // The current window that we are drawing to.
        // When no window is currently in use (outside beforeDraw - afterDraw)
        // this is set to NULL.
        Window * window;
        
        // The device context for the display we are currently drawing to -
        // this is required for painting operations.
        // When no window is currently in use (outside beforeDraw - afterDraw)
        // this is set to NULL.
        HDC displayContext;
        
    public:
        /**
         * Note that it is the responsibility of subclasses to set properties
         * like normal component sizes, ui border, custom colours etc.
         */
        WinThemeBase(void);
        virtual ~WinThemeBase(void);

    protected:
        // -------------------- Accessor methods for subclasses ----------------
        
        /**
         * Gets the current context that we are using.
         * When no context is currently in use (outside init - destroy) this
         * returns NULL.
         */
        inline Context * getContext(void) const {
            return context;
        }
        
        /**
         * Gets the current window that we are drawing to.
         * When no window is currently in use (outside beforeDraw - afterDraw)
         * this returns NULL.
         */
        inline Window * getWindow(void) const {
            return window;
        }
        
        /**
         * Accesses the device context for the display we are currently drawing
         * to - this is required for painting operations.
         * When no window is currently in use (outside beforeDraw - afterDraw)
         * this is set to NULL.
         */
        inline HDC getDisplayContext(void) const {
            return displayContext;
        }
    
        // ------------------------ Theme implementation -----------------------
        
        // Methods we override to provide the required operations like double buffering.
        virtual void init(void * userData) override;
    public:
        virtual void beforeDraw(void * userData) override;
        virtual void afterDraw(const rascUI::Rectangle & drawnArea) override;
    protected:
        virtual void destroy(void) override;
    public:
        virtual void onChangeViewArea(const rascUI::Rectangle & view) override;
        
        /**
         * Returns a pointer to our current display context, for custom drawing.
         * When no window is currently in use (outside beforeDraw - afterDraw)
         * this is set to NULL.
         */
        virtual void * getDrawBuffer(void) override;
        
        // Methods we override and do nothing, as to allow use in a ThemeGroup.
        virtual void redrawFromBuffer(void) override;
        virtual void drawBackPanel(const rascUI::Location & location) override;
        virtual void drawFrontPanel(const rascUI::Location & location) override;
        virtual void drawText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawButton(const rascUI::Location & location) override;
        virtual void drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) override;
        virtual void drawScrollBarBackground(const rascUI::Location & location) override;
        virtual void drawScrollContentsBackground(const rascUI::Location & location) override;
        virtual void drawScrollPuck(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollUpButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollDownButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawFadePanel(const rascUI::Location & location) override;
        virtual void drawCheckboxButton(const rascUI::Location & location) override;
        virtual void drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) override;
        virtual void drawTooltipBackground(const rascUI::Location & location) override;
        virtual void drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) override;
        virtual void drawWindowDecorationBackPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationFrontPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) override;
        
        virtual void customSetDrawColour(void * colour) override;
        virtual void * customGetTextColour(const rascUI::Location & location) override;
        virtual void customDrawTextMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) override;
        virtual void customDrawTextNoMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) override;
        virtual void customDrawTextTitle(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) override;
        virtual void getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) override;
    };
}

#endif
