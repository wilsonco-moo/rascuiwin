/*
 * WinThemeBase.cpp
 *
 *  Created on: 18 May 2020
 *      Author: wilson
 */

#include "WinThemeBase.h"

#include <rascUI/base/TopLevelContainer.h>
#include <windows.h>
#include <cstddef>

#include "../platform/Context.h"
#include "../platform/Window.h"

namespace rascUIwin {

    WinThemeBase::WinThemeBase(void) :
        context(NULL),
        window(NULL),
        displayContext(NULL) {
    }

    WinThemeBase::~WinThemeBase(void) {
    }

    void WinThemeBase::init(void * userData) {
        // Here, the user data is the context.
        context = (Context *)userData;
    }
    
    void WinThemeBase::beforeDraw(void * userData) {
        // Here, the user data is the window.
        window = (Window *)userData;
        // Create a display context from either the off screen buffer or the
        // window itself, depending on double buffer status.
        if (window->winIsDoubleBufferEnabled()) {
            // If the window has an off-screen buffer, get the window's display context, create a compatible one for
            // us to draw to, select the bitmap and release the window's display context. This creates a display
            // context which draws to the bitmap.
            HBITMAP offScreenBuffer = window->winGetOffScreenBuffer();
            if (offScreenBuffer != NULL) {
                HDC windowDisplayContext = GetDC(window->winGetHandle());
                displayContext = CreateCompatibleDC(windowDisplayContext);
                ReleaseDC(window->winGetHandle(), windowDisplayContext);
                SelectObject(displayContext, offScreenBuffer);
            }
        } else {
            displayContext = GetDC(window->winGetHandle());
        }
    }
    
    void WinThemeBase::afterDraw(const rascUI::Rectangle & drawnArea) {
        // If double buffering is enabled, we need to copy the off-screen buffer
        // to the window.
        if (window->winIsDoubleBufferEnabled()) {
            
            // Clip the draw area with the TopLevelContainer last view, so we don't go out of bounds.
            rascUI::Rectangle copyArea = drawnArea * window->getLastView();
            
            // We can always assume that displayContext exists, since onChangeViewArea will
            // always have been called. Get a display context for the window, copy the area, then
            // release the window display context and delete the display context from the bitmap.
            // Only do the copy if the drawn area actually exists.
            if (copyArea.isNonZero()) {
                HDC windowDisplayContext = GetDC(window->winGetHandle());
                BitBlt(windowDisplayContext,                        // Destination display context.
                       (int)copyArea.x, (int)copyArea.y,            // Destination x,y.
                       (int)copyArea.width, (int)copyArea.height,   // Area width, height.
                       displayContext,                              // Source display context.
                       (int)copyArea.x, (int)copyArea.y,            // Source x,y.
                       SRCCOPY);                                   // Mode: direct copy.
                ReleaseDC(window->winGetHandle(), windowDisplayContext);
            }
            DeleteDC(displayContext);
            
        // Otherwise, we simply need to release the display context.
        } else {
            ReleaseDC(window->winGetHandle(), displayContext);
        }
        
        // After finishing drawing, relinquish anything relating to the window.
        window = NULL;
        displayContext = NULL;
    }
    
    void WinThemeBase::destroy(void) {
        // After destroy, relinquish anything relating to the context.
        context = NULL;
    }
    
    void WinThemeBase::onChangeViewArea(const rascUI::Rectangle & view) {
        // If the window has double buffering enabled, we need to resize
        // the off screen buffer.
        if (window->winIsDoubleBufferEnabled()) {
            
            // If we created a display context from the bitmap in beforeDraw (the window had an existing buffer), delete it.
            if (displayContext != NULL) {
                DeleteDC(displayContext);
            }
            
            // If the window had an existing off screen buffer, delete it.
            HBITMAP offScreenBuffer = window->winGetOffScreenBuffer();
            if (offScreenBuffer != NULL) {
                DeleteObject(offScreenBuffer);
            }
            
            // Create a new off screen buffer, compatible with the window's display context.
            HDC windowDisplayContext = GetDC(window->winGetHandle());
            offScreenBuffer = CreateCompatibleBitmap(windowDisplayContext, (int)(view.x + view.width), (int)(view.y + view.height));
            
            // Create a compatible display context and select the bitmap into it. Release the window display context while we are at it.
            displayContext = CreateCompatibleDC(windowDisplayContext);
            ReleaseDC(window->winGetHandle(), windowDisplayContext);
            SelectObject(displayContext, offScreenBuffer);
            
            // Store the off screen buffer back in the window, for next time.
            window->winGetOffScreenBuffer() = offScreenBuffer;
        }
    }

    void * WinThemeBase::getDrawBuffer(void) {
        return &displayContext;
    }

    // Methods we override and do nothing, as to allow use in a ThemeGroup.
    void WinThemeBase::redrawFromBuffer(void) {}
    void WinThemeBase::drawBackPanel(const rascUI::Location & location) {}
    void WinThemeBase::drawFrontPanel(const rascUI::Location & location) {}
    void WinThemeBase::drawText(const rascUI::Location & location, const std::string & string) {}
    void WinThemeBase::drawButton(const rascUI::Location & location) {}
    void WinThemeBase::drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) {}
    void WinThemeBase::drawScrollBarBackground(const rascUI::Location & location) {}
    void WinThemeBase::drawScrollContentsBackground(const rascUI::Location & location) {}
    void WinThemeBase::drawScrollPuck(const rascUI::Location & location, bool vertical) {}
    void WinThemeBase::drawScrollUpButton(const rascUI::Location & location, bool vertical) {}
    void WinThemeBase::drawScrollDownButton(const rascUI::Location & location, bool vertical) {}
    void WinThemeBase::drawFadePanel(const rascUI::Location & location) {}
    void WinThemeBase::drawCheckboxButton(const rascUI::Location & location) {}
    void WinThemeBase::drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) {}
    void WinThemeBase::drawTooltipBackground(const rascUI::Location & location) {}
    void WinThemeBase::drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) {}
    void WinThemeBase::drawWindowDecorationBackPanel(const rascUI::Location & location) {}
    void WinThemeBase::drawWindowDecorationFrontPanel(const rascUI::Location & location) {}
    void WinThemeBase::drawWindowDecorationText(const rascUI::Location & location, const std::string & string) {}
    void WinThemeBase::drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) {}

    void WinThemeBase::customSetDrawColour(void * colour) {}
    void * WinThemeBase::customGetTextColour(const rascUI::Location & location) { return NULL; }
    void WinThemeBase::customDrawTextMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string)  {}
    void WinThemeBase::customDrawTextNoMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {}
    void WinThemeBase::customDrawTextTitle(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {}
    void WinThemeBase::getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) {}
}
