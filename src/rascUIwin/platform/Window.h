/*
 * Window.h
 *
 *  Created on: 18 May 2020
 *      Author: wilson
 */

#ifndef RASCUIWIN_PLATFORM_WINDOW_H_
#define RASCUIWIN_PLATFORM_WINDOW_H_

#include <rascUI/platform/WindowBase.h>
#include <windef.h>
#include <string>

namespace rascUIwin {
    class Context;

    /**
     * The rascUIwin implementation of a rascUI platform Window.
     */
    class Window : public rascUI::WindowBase {
    private:
        // Our window class name. This is generated unique for each window.
        std::string windowClassName;
        
        // Set to true if we managed to successfully register ourself as a window.
        bool successfullyRegistered;
    
        // The struct representing our unique window class. This must
        // be initialised with a unique name.
        WNDCLASSEX windowClass;
        
        // The atom returned when registering our window class.
        ATOM windowClassAtom;
        
        // The handle for our window, once it is created with CreateWindow.
        HWND windowHandle;
        
        // Whether double buffering should be enabled for this window.
        // Note that double buffering, (and whether to comply with this),
        // is entirely implemented by themes.
        bool doubleBufferEnabled;
        
        // This should be used by themes for an off screen buffer to draw to.
        // If this exists (i.e: is not NULL) when the window is
        // destroyed, the window will automatically free this.
        // This is set to NULL by default, and is intended to be
        // modified by themes.
        HBITMAP offScreenBuffer;
        
        // Stores the most recent mouse move x and y position. This is used for some mouse events
        // (such as WM_MOUSEWHEEL), where for some reason windows provides us with the wrong
        // mouse position.
        int lastMouseMoveX, lastMouseMoveY;
        
        // Stores whether we are currently tracking the mouse. This is used by
        // Context for finding out when the mouse leaves the window.
        bool isTrackingMouse;
        
    public:
        /**
         * There are no custom options for rascUIwin::Window.
         */
        Window(Context * context, const rascUI::WindowConfig & config,
               rascUI::Theme * theme = NULL, const rascUI::Location & location = rascUI::Location(),
               const rascUI::Bindings * bindings = NULL,
               const GLfloat * xScalePtr = NULL, const GLfloat * yScalePtr = NULL);
               
        virtual ~Window(void);
        
    private:
        // Our WindowProc callback function. This is used to provide messages to the window.
        // Messages cannot be handled directly within the main loop as, for some reason,
        // about half of messages seem to skip the callback system completely and run this function directly.
        // See: https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/ms633573(v=vs.85)
        //      http://web.archive.org/web/20200501032336/https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/ms633573(v=vs.85)
        static LRESULT windowMessage(HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam);
        
    protected:
        /**
         * Here we simply show the window, using the "nCmdShow" option from
         * our Context.
         */
        virtual void onMapWindow(void) override;
        /**
         * Here we hide the window, (strangely, also using the ShowWindow function).
         */
        virtual void onUnmapWindow(void) override;
        
    public:
        // ----------------- Windows API specific methods ----------------------
        
        /**
         * Allows access to our window handle.
         */
        inline HWND winGetHandle(void) const {
            return windowHandle;
        }
        
        /**
         * Returns whether double buffering is enabled.
         */
        inline bool winIsDoubleBufferEnabled(void) const {
            return doubleBufferEnabled;
        }
        
        /**
         * Allows access to, and modification of, our off screen buffer
         * bitmap. (See comments for offScreenBuffer field).
         */
        inline HBITMAP & winGetOffScreenBuffer(void) {
            return offScreenBuffer;
        }
        
        /**
         * Allows accessing and modifying the last mouse move X position. This is used for some
         * mouse events (such as WM_MOUSEWHEEL), where for some reason windows provides us with
         * the wrong mouse position.
         */
        inline int & winGetLastMouseMoveX(void) {
            return lastMouseMoveX;
        }
        
        /**
         * Allows accessing and modifying the last mouse move Y position. This is used for some
         * mouse events (such as WM_MOUSEWHEEL), where for some reason windows provides us with
         * the wrong mouse position.
         */
        inline int & winGetLastMouseMoveY(void) {
            return lastMouseMoveY;
        }
        
        /**
         * This should be used by Context for repainting us.
         * This first gets our window geometry using GetClientRect, then uses that for the view area.
         */
        void winRepaintIfNeeded(void);
        
        /**
         * Accesses/modifies whether we are currently tracking the mouse. This is used by
         * Context for finding out when the mouse leaves the window.
         */
        inline bool & winIsTrackingMouse(void) {
            return isTrackingMouse;
        }
    };
}

#endif
