/*
 * Context.h
 *
 *  Created on: 18 May 2020
 *      Author: wilson
 */

#ifndef RASCUIWIN_PLATFORM_CONTEXT_H_
#define RASCUIWIN_PLATFORM_CONTEXT_H_

#include <rascUI/platform/ContextBase.h>
#include <unordered_set>
#include <windef.h>

namespace rascUIwin {
    class Window;

    /**
     * In rascUIwin, Context holds anything which is not specific to a
     * particular window, such as the hInstance and startup info. It also keeps
     * track of the current windows which exist.
     * 
     * In windows, the hInstance represents the base address of the currently
     * running program in memory.
     */
    class Context : public rascUI::ContextBase {
    private:
        // This allows windows to run the registerWindow/unregisterWindow methods.
        friend class Window;
    
        // Our hInstance: the base address of the currently running program
        // in memory.
        HINSTANCE hInstance;
        
        // Whether to continue executing the main loop. This is set to false by endMainLoop.
        bool continueMainLoop;
        
        // Used by window instances to notify of errors. This should be set by notifyWindowError.
        bool windowError;
        
        // This allows Window instances to request a unique number, so they can each
        // have a unique class name (using the getUniqueClassNumber method).
        unsigned long long classNumberUpto;
        
        // A set of windows related to THIS context.
        std::unordered_set<Window *> windowHandles;
        
        // The startup info of the program.
        STARTUPINFO startupInfo;
        
        // Set by themes using winThemeSetBackground, for drawing the background colour of ALL WINDOWS related
        // to this context. If both of these are non-NULL values, we will intercept WM_ERASEBKGND messages and
        // use these to draw the background - but only for windows which are not double buffered.
        HPEN backgroundPen;
        HBRUSH backgroundBrush;
        
        // The cumulative scroll delta - for mice with free-floating scroll wheels.
        int scrollCumulative;
        
    public:
        /**
         * 
         */
        Context(rascUI::Theme * defaultTheme,
                const GLfloat * defaultXScalePtr = NULL,
                const GLfloat * defaultYScalePtr = NULL,
                const rascUI::Bindings * defaultBindings = NULL);
        
        virtual ~Context(void);
        
    private:
        // Run by Window at the end of it's constructor.
        void registerWindow(Window * window);
        
        // Run by Window at the start of it's destructor.
        void unregisterWindow(Window * window);
    
        // Returns the number of windows which are currently mapped
        // (shown in windows terminology).
        size_t countMappedWindows(void) const;
        
        // Returns a unique number each time. Window classes should use this
        // to generate themselves a unique class name.
        unsigned long long getUniqueClassNumber(void);
        
        // Window instances should use this to notify that there has been some
        // kind of error in their initialisation (etc).
        inline void notifyWindowError(void) {
            windowError = true;
        }
        
        // This should be run by associated window instances, when they get messages through
        // their event callback function.
        LRESULT runWindowMessage(Window * window, HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam);
        
        // Gets a message, optionally with a timeout.
        // If the timeout is NULL, this acts exactly like GetMessage.
        // If the timeout is zero, this returns immediately using PeekMessage.
        // In all cases, this returns -1 (and sets createdMessage to false) on error.
        // createdMessage is set to true when a message is read, and set to false if we timed out first.
        // Note that this ignores (sets createdMessage to false) in the case of all WM_TIMER messages.
        BOOL getMessageTimeout(MSG * msg, bool * createdMessage, unsigned long long * timeout);
        
    public:
        /**
         * Here we return true if accessing our hInstance succeeded, AND
         * none of our window instances have notified us of any errors,
         * using notifyWindowError.
         */
        virtual bool wasSuccessful(void) const override;
        
        /**
         * This runs UpdateWindow on all mapped windows.
         */
        virtual void flush(void) override;
        
        /**
         * Our implementation of the main event loop. This waits for events
         * sent to our thread, and dispatches them to the appropriate windows.
         */
        virtual unsigned long long mainLoop(void) override;

        /**
         * Ends the main event loop.
         */
        virtual void endMainLoop(void) override;
        
        /**
         * Repaints all (mapped) windows.
         */
        virtual void repaintEverything(void) override;
        
        // ----------------- Windows API specific methods ----------------------
        
        /**
         * Gets the "nCmdShow" option, from the startup info.
         * This is a hint from outside applications as to how our windows should
         * be displayed - for example whether they should start minimised,
         * maximised, fullscreen etc.
         * See documentation for wShowWindow field here:
         * https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/ns-processthreadsapi-startupinfoa
         * http://web.archive.org/web/20200508093504/https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/ns-processthreadsapi-startupinfoa
         * Also see documentation for ShowWindow here:
         * https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-showwindow
         * http://web.archive.org/web/20190906223326/https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-showwindow
         */
        inline int winGetNCmdShow(void) const {
            return startupInfo.wShowWindow;
        }
        
        /**
         * Allows access to our hInstance. This is the base address of the
         * currently running program in memory, and is used by many windows
         * functions.
         */
        inline HINSTANCE winGetHInstance(void) const {
            return hInstance;
        }
        
        /**
         * This can be used by themes to set the background pen and brush for
         * ALL (non double buffered) WINDOWS relating to this context.
         * If these are set to non-NULL values, we will intercept WM_ERASEBKGND
         * messages and use these to draw the background - but only for windows
         * which are not double buffered.
         * 
         * If the Theme uses this, it should set these in its init method, and
         * reset them to NULL in its destroy method.
         */
        void winThemeSetBackground(HPEN pen, HBRUSH brush);
        
        /**
         * Returns the Window instance which is related to the specified window
         * handle. This will only return something if the window is currently
         * related to a Context. This accesses a static data structure, and is
         * used by all Context instances which currently exist.
         * Returns NULL if the window cannot be found.
         * 
         * This is required to get around the fact that WindowProc callback
         * system which Windows forces you to use, has no way to specify
         * any kind of user data - forcing you to store things statically.
         * 
         * This method locks to a mutex internally, and is thread safe.
         * This allows multiple different Context instances to be used on
         * different threads.
         */
        static Window * winGetWindowFromHandle(HWND windowHandle);
        
        /**
         * Interprets the key code from the wParam and lParam of a WM_KEYUP or
         * WM_KEYDOWN message.
         * 
         * valid     Is set to true if this key message should be used. It is set to false if either
         *           something went wrong interpreting the key code, or if the key is a "dead key" (accent or diacritic).
         * special   If valid is set to true, this is set to false for normal printable characters, and set to true
         *           for non-printable characters like the arrow keys, or ctrl/shift etc.
         * character If valid is set to true, this is set to the appropriate character code.
         */
        static void winInterpretVirtualKeycode(bool * valid, bool * special, int * character, WPARAM wParam, LPARAM lParam);
    };
}

#endif
