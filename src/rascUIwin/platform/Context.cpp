/*
 * Context.cpp
 *
 *  Created on: 18 May 2020
 *      Author: wilson
 */

#include "Context.h"

#include <rascUI/util/Bindings.h>
#include <rascUI/base/Theme.h>
#include <unordered_map>
#include <windowsx.h>
#include <windows.h>
#include <iostream>
#include <mutex>

#include "Window.h"

namespace rascUIwin {
    
    // This data structure maps window handles to Window instances. The mutex makes access
    // thread safe. These are accessed from registerWindow, unregisterWindow, and winGetWindowFromHandle.
    static std::mutex staticMutex;
    static std::unordered_map<HWND, Window *> windowHandleMap;
    
    

    Context::Context(rascUI::Theme * defaultTheme, const GLfloat * defaultXScalePtr, const GLfloat * defaultYScalePtr, const rascUI::Bindings * defaultBindings) :
        rascUI::ContextBase(defaultTheme,
                            (defaultBindings == NULL) ? &rascUI::Bindings::defaultWin : defaultBindings,
                            defaultYScalePtr, defaultYScalePtr, true),
        
        // Get the hInstance - this might fail and return NULL.
        hInstance(GetModuleHandle(NULL)),
        windowError(false),
        classNumberUpto(0),
        windowHandles(),
        backgroundPen(NULL),
        backgroundBrush(NULL),
        scrollCumulative(0) {
        
        // Get the startup info of the program.
        GetStartupInfo(&startupInfo);
        
        // Only call the Theme's init method if loading the platform was successful, and if
        // we actually have a Theme. Note that the user data is a Context: this.
        if (defaultTheme != NULL && hInstance != NULL) {
            defaultTheme->callInit(this);
        }
    }
    
    Context::~Context(void) {
        // Neither the hInstance nor the startup contain anything which needs freeing.
        // Call the Theme's destroy, if we have a Theme.
        if (defaultTheme != NULL) {
            defaultTheme->callDestroy();
        }
    }
    
    void Context::registerWindow(Window * window) {
        // Complain if the window handle is not valid.
        HWND windowHandle = window->winGetHandle();
        if (windowHandle == 0) {
            std::cerr << "WARNING: rascUIwin::Context: " << "Tried to register window with invalid handle.\n";
            return;
        }
        // Put the window in our local data structure.
        windowHandles.insert(window);
        // Lock to mutex and put the window in the static data structure. Complain if it already existed.
        std::lock_guard<std::mutex> lock(staticMutex);
        auto iter = windowHandleMap.find(windowHandle);
        if (iter == windowHandleMap.end()) {
            windowHandleMap.emplace(windowHandle, window);
        } else {
            std::cerr << "WARNING: rascUIwin::Context: " << "Tried to register a window which has already been registered.\n";
        }
    }

    void Context::unregisterWindow(Window * window) {
        // Complain if the window handle is not valid.
        HWND windowHandle = window->winGetHandle();
        if (windowHandle == 0) {
            std::cerr << "WARNING: rascUIwin::Context: " << "Tried to unregister window with invalid handle.\n";
            return;
        }
        windowHandles.erase(window);
        // Lock to the mutex and erase the window from the static data structure. Complain if it didn't exist.
        std::lock_guard<std::mutex> lock(staticMutex);
        auto iter = windowHandleMap.find(windowHandle);
        if (iter == windowHandleMap.end()) {
            std::cerr << "WARNING: rascUIwin::Context: " << "Tried to unregister a window which was not registered.\n";
        } else {
            windowHandleMap.erase(iter);
        }
    }
    
    size_t Context::countMappedWindows(void) const {
        size_t count = 0;
        for (Window * window : windowHandles) {
            count += window->isMapped();
        }
        return count;
    }
    
    unsigned long long Context::getUniqueClassNumber(void) {
        return classNumberUpto++;
    }
    
    bool Context::wasSuccessful(void) const {
        // GetModuleHandle returns NULL upon failure to access the hInstance.
        // We must not have been notified of errors by any windows.
        return hInstance != NULL && !windowError;
    }
    
    void Context::flush(void) {
        for (Window * window : windowHandles) {
            if (window->isMapped()) {
                UpdateWindow(window->winGetHandle());
            }
        }
    }
    
    unsigned long long Context::mainLoop(void) {
        // Ignore any calls to endMainLoop which happened outside mainLoop, flush the connection to initially paint windows.
        continueMainLoop = true;
        flush();
        
        // Initially update function queues before we start waiting for messages.
        beforeEvent.update();
        afterEvent.update();
        
        {   
            // Peek a message and discard it, to ensure that we definately have a message queue set up.
            MSG msg;
            PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);
        
            // Now we are sure that we have a message queue, set the update thread IDs of the function queues.
            DWORD threadId = GetCurrentThreadId();
            beforeEvent.setUpdateThreadId(threadId);
            afterEvent.setUpdateThreadId(threadId);
        }
        
        unsigned long long eventCount = 0;
        
        // Loop until it is requested that we stop (from endMainLoop), we run out of mapped windows, or any windows report errors.
        while(continueMainLoop && countMappedWindows() > 0 && !windowError) {
            
            // Ask the function queues how long until their next timed event.
            unsigned long long timeNow = (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch())).count(),
                               minTimeUntil = (unsigned long long)-1;
            bool hasWaiting = false;
            beforeEvent.hasWaitingTimedFunctions(timeNow, &minTimeUntil, &hasWaiting);
            afterEvent.hasWaitingTimedFunctions(timeNow, &minTimeUntil, &hasWaiting);
            
            // Get a message, using the minimum timeout from the function queues.
            MSG message;
            bool createdMessage;
            BOOL result = getMessageTimeout(&message, &createdMessage, hasWaiting ? &minTimeUntil : NULL);
            
            // If there was an error in getting the message, end the main loop. Otherwise increment the event count.
            if (result == -1) {
                std::cerr << "WARNING: rascUIwin::Context: " << "Failed to read message from message queue, error code: " << GetLastError() << ".\n";
                beforeEvent.update(); // The before event function queue ALWAYS gets processed before the message, even if there is an error.
                break; // Some kind of generic error: end main event loop.
            
            } else if (result == 0) {
                // WM_QUIT message.
                // https://docs.microsoft.com/en-gb/windows/win32/winmsg/wm-quit
                // TODO: handle.
                std::cout << "Quit?\n";
            }
            eventCount++;
            
            // If a message got created, use the windows callback system to dispatch the message.
            // This results in runWindowMessage being called, in a long and convoluted way.
            // We can't do the sensible thing here and handle messages in the main loop, as about
            // half the messages go straight to the window callback without using the GetMessage/DispatchMessage system.
            if (createdMessage) {
                DispatchMessage(&message);
            
            // If a message was not created, we can assume that we were interrupted by a timeout.
            // So update the function queues and repaint any windows which want repainting.
            } else {
                beforeEvent.update();
                afterEvent.update();
                for (Window * window : windowHandles) {
                    window->winRepaintIfNeeded();
                }
            }            
        }
        
        // Reset the update thread IDs of the function queues, now the main loop is done.
        beforeEvent.setUpdateThreadId();
        afterEvent.setUpdateThreadId();
        
        // Make sure this gets updated, even on the last iteration after exiting the loop.
        afterEvent.update();
        
        return eventCount;
    }
    
    LRESULT Context::runWindowMessage(Window * window, HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam) {
        LRESULT ret = 0;
        // Can be set to false - some messages explicitly require that no repainting happens.
        bool doRepaint = true; 
        
        // Update before event function queue before updating the window.
        beforeEvent.update();
        
        switch(message) {
        
        case WM_PAINT:
            // Run the default procedure: according to documentation, this validates the update region and stops WM_PAINT being called again immediately.
            // See: https://docs.microsoft.com/en-us/windows/win32/gdi/wm-paint
            //      http://web.archive.org/web/20190821181311/https://docs.microsoft.com/en-us/windows/win32/gdi/wm-paint
            DefWindowProc(windowHandle, message, wParam, lParam);
            // Since this is an outside paint function, tell the rascUI component hierarchy to repaint itself completely.
            window->repaint();           
            break;
        
        case WM_DESTROY:
            // Ignore destroy messages.
            break;
        
        case WM_QUIT:
            // Ignore quit messages.
            break;
        
        case WM_CLOSE:
            // Run the close request method, when the user requests the window to close.
            window->onUserRequestClose();
            break;
        
        case WM_ERASEBKGND:
            // If the window is double buffered, purposefully do nothing, and tell windows
            // that we have done something. This stops ANY background from being drawn - avoiding flickering.
            if (window->winIsDoubleBufferEnabled()) {
                doRepaint = false;
                ret = 1;
                
            // If the theme has not set a background pen/brush, use the default window procedure.
            } else if (backgroundBrush == NULL || backgroundPen == NULL) {
                ret = DefWindowProc(windowHandle, message, wParam, lParam);
            } else {
                RECT clientRect;
                // If getting the window size failed, complain and also use default window procedure.
                if (GetClientRect(windowHandle, &clientRect) == 0) {
                    std::cerr << "WARNING: rascUIwin::Context: " << "Failed to access window size, error code: " << GetLastError() << ".\n";
                    ret = DefWindowProc(windowHandle, message, wParam, lParam);
                    
                // If we successfully got the window size, fill it with a rectangle. Force a repaint not to happen,
                // and (as required in the Windows API documentation) return a non zero value.
                // Note that for WM_ERASEBKGND events, oddly, the device context (pointer!) is passed as wParam (integer!) - hence the ugly cast.
                // See https://docs.microsoft.com/en-us/windows/win32/gdi/window-background
                //     http://web.archive.org/web/20200520131324/https://docs.microsoft.com/en-us/windows/win32/gdi/window-background
                } else {
                    doRepaint = false;
                    ret = 1;
                    SelectObject((HDC)wParam, backgroundPen);
                    SelectObject((HDC)wParam, backgroundBrush);
                    Rectangle((HDC)wParam, clientRect.left, clientRect.top, clientRect.right, clientRect.bottom);
                }
            }
            break;
        
        case WM_MOUSEMOVE: {
            // Start mouse tracking in the window, if it isn't already.
            if (!window->winIsTrackingMouse()) {
                window->winIsTrackingMouse() = true;
                TRACKMOUSEEVENT mouseTrack;
                mouseTrack.cbSize = sizeof(TRACKMOUSEEVENT);
                mouseTrack.dwFlags = TME_LEAVE;
                mouseTrack.hwndTrack = windowHandle;
                mouseTrack.dwHoverTime = HOVER_DEFAULT;
                if (TrackMouseEvent(&mouseTrack) == 0) {
                    std::cerr << "WARNING: rascUIwin::Context: " << "Failed to track mouse events, error code: " << GetLastError() << ".\n";
                }
            }
            
            // Save the mouse x,y and notify the window of the mouse position.
            int x = GET_X_LPARAM(lParam),
                y = GET_Y_LPARAM(lParam);
            window->winGetLastMouseMoveX() = x;
            window->winGetLastMouseMoveY() = y;
            if (window->mouseMove(x, y)) {
                window->onUnusedMouseMove(x, y);
            }
            break;
        }
        
        case WM_LBUTTONDOWN: // Left mouse button is 0 (consistent with GLUT/OpenGL, see rascUI/util/Bindings.h).
            if (window->mouseEvent(0, rascUI::MouseEvents::PRESS, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))) {
                window->onUnusedMouseEvent(0, rascUI::MouseEvents::PRESS, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
            }
            break;
        
        case WM_LBUTTONUP: // Left mouse button is 0 (consistent with GLUT/OpenGL, see rascUI/util/Bindings.h).
            if (window->mouseEvent(0, rascUI::MouseEvents::RELEASE, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))) {
                window->onUnusedMouseEvent(0, rascUI::MouseEvents::RELEASE, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
            }
            break;
        
        case WM_MBUTTONDOWN: // Middle mouse button is 1 (consistent with GLUT/OpenGL, see rascUI/util/Bindings.h).
            if (window->mouseEvent(1, rascUI::MouseEvents::PRESS, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))) {
                window->onUnusedMouseEvent(1, rascUI::MouseEvents::PRESS, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
            }
            break;
        
        case WM_MBUTTONUP: // Middle mouse button is 1 (consistent with GLUT/OpenGL, see rascUI/util/Bindings.h).
            if (window->mouseEvent(1, rascUI::MouseEvents::RELEASE, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))) {
                window->onUnusedMouseEvent(1, rascUI::MouseEvents::RELEASE, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
            }
            break;
        
        case WM_RBUTTONDOWN: // Right mouse button is 2 (consistent with GLUT/OpenGL, see rascUI/util/Bindings.h).
            if (window->mouseEvent(2, rascUI::MouseEvents::PRESS, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))) {
                window->onUnusedMouseEvent(2, rascUI::MouseEvents::PRESS, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
            }
            break;
        
        case WM_RBUTTONUP: // Right mouse button is 2 (consistent with GLUT/OpenGL, see rascUI/util/Bindings.h).
            if (window->mouseEvent(2, rascUI::MouseEvents::RELEASE, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))) {
                window->onUnusedMouseEvent(2, rascUI::MouseEvents::RELEASE, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
            }
            break;
        
        case WM_MOUSEWHEEL: {
            // Scroll up and down are 3 and 4 (for compatibility with OpenGL/GLUT). Note that (for some reason) windows provides us
            // with a completely wrong mouse position here, so use the most recent mouse move x,y from the window.
            
            int scrollDelta = GET_WHEEL_DELTA_WPARAM(wParam);
            scrollCumulative += scrollDelta;
            
            int mouseButton = -1;
            if (scrollCumulative <= -WHEEL_DELTA) {
                mouseButton = 4;
                
            } else if (scrollCumulative >= WHEEL_DELTA) {
                mouseButton = 3;
            
            }
            
            scrollCumulative %= WHEEL_DELTA;
            
            if (mouseButton != -1) {
                int x = window->winGetLastMouseMoveX(),
                    y = window->winGetLastMouseMoveY();
                if (window->mouseEvent(mouseButton, rascUI::MouseEvents::PRESS, x, y)) {
                    window->onUnusedMouseEvent(mouseButton, rascUI::MouseEvents::PRESS, x, y);
                }
                if (window->mouseEvent(mouseButton, rascUI::MouseEvents::RELEASE, x, y)) {
                    window->onUnusedMouseEvent(mouseButton, rascUI::MouseEvents::RELEASE, x, y);
                }
            }
            break;
        }
        
        case WM_KEYDOWN: {
            bool valid, special;
            int character;
            winInterpretVirtualKeycode(&valid, &special, &character, wParam, lParam);
            if (valid) {
                if (window->keyPress(character, special)) {
                    window->onUnusedKeyPress(character, special);
                }
            }
            break;
        }
        
        case WM_KEYUP: {
            bool valid, special;
            int character;
            winInterpretVirtualKeycode(&valid, &special, &character, wParam, lParam);
            if (valid) {
                if (window->keyRelease(character, special)) {
                    window->onUnusedKeyRelease(character, special);
                }
            }
            break;
        }
        
        case WM_MOUSELEAVE: {
            // Reset the status of tracking the mouse.
            window->winIsTrackingMouse() = false;
            // When we get a leave notify, give the UI system a mouse move event to a position outside the window
            // (this is guaranteed by using the top level container location cache's outsideX and outsideY methods,
            // see rascUI::Rectangle).
            GLfloat x = window->location.cache.outsideX(),
                    y = window->location.cache.outsideY();
            if (window->mouseMove(x, y)) {
                window->onUnusedMouseMove(x, y);
            }
            break;
        }
        
        default:
            ret = DefWindowProc(windowHandle, message, wParam, lParam);
            break;
        }
        
        // The afterEvent function queue must be updated BEFORE repainting: actions may request that stuff gets repainted.
        afterEvent.update();
        
        // Repaint the window if no events have explicitly refused a repaint.
        if (doRepaint) window->winRepaintIfNeeded();
        
        return ret;
    }
    
    BOOL Context::getMessageTimeout(MSG * msg, bool * createdEvent, unsigned long long * requestedTimeout) {
        // For NULL timeouts (no timeout), just use GetMessage. We created an event if it did not return -1, and the message is not WM_TIMER.
        if (requestedTimeout == NULL) {
            BOOL result = GetMessage(msg, NULL, 0, 0);
            *createdEvent = (result != -1 && msg->message != WM_TIMER);
            return result;
        }
        
        // In the case of zero timeout, use peek message. We created an event if it did not return 0, and the message is not WM_TIMER.
        // Return 1 since this never fails.
        if (*requestedTimeout == 0) {
            BOOL result = PeekMessage(msg, NULL, 0, 0, PM_REMOVE);
            *createdEvent = (result != 0 && msg->message != WM_TIMER);
            return 1;
        }
        
        // If the user requested a timeout which is bigger than windows can do, use the maximum.
        // This avoids integer overflow.
        UINT timeout;
        if (*requestedTimeout > (unsigned long long)USER_TIMER_MINIMUM) {
            timeout = USER_TIMER_MINIMUM;
        } else {
            timeout = (UINT)(*requestedTimeout);
        }
        
        // Set a timer to interrupt the GetMessage call after the timeout has elapsed, get message then kill the timer.
        UINT_PTR timerId = SetTimer(NULL, 0, timeout, NULL);
        BOOL result = GetMessage(msg, NULL, 0, 0);
        KillTimer(NULL, timerId);
    
        // We created an event if it did not return -1, and the message is not WM_TIMER.
        *createdEvent = (result != -1 && msg->message != WM_TIMER);
        return result;
    }
    
    void Context::endMainLoop(void) {
        continueMainLoop = false;
    }
    
    void Context::repaintEverything(void) {
        for (Window * window : windowHandles) {
            if (window->isMapped()) {
                window->repaint();
            }
        }
    }
    
    void Context::winThemeSetBackground(HPEN pen, HBRUSH brush) {
        backgroundPen = pen;
        backgroundBrush = brush;
    }
    
    Window * Context::winGetWindowFromHandle(HWND windowHandle) {
        std::lock_guard<std::mutex> lock(staticMutex);
        auto iter = windowHandleMap.find(windowHandle);
        if (iter == windowHandleMap.end()) {
            return NULL;
        } else {
            return iter->second;
        }
    }
    
    void Context::winInterpretVirtualKeycode(bool * valid, bool * special, int * character, WPARAM wParam, LPARAM lParam) {
        // This assumes that windows's UINT type is the same size as an int.
        static_assert(sizeof(UINT) == sizeof(int));
        
        // See https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-keydown
        //     http://web.archive.org/web/20200128040642/https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-keydown
        
        // Virtual keycode is wParam, scan code is lParam bits 16-23
        UINT virtualKeycode = (UINT)wParam,
             scanCode = (lParam >> 16) & 255;
        
        // Get the current keyboard state, complain if there is an error.
        BYTE keyboardState[256];
        if (GetKeyboardState(keyboardState) == 0) {
            std::cerr << "WARNING: rascUIwin::Context: " << "Failed to accesss keyboard state, error code: " << GetLastError() << ".\n";
            *valid = false;
        }
        
        // See: https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-tounicode
        //      http://web.archive.org/web/20200522141230/https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-tounicode
        
        // Do the conversion, and write to a WCHAR (size 1).
        WCHAR output;
        int result = ToUnicode(virtualKeycode, scanCode, keyboardState, &output, 1, 0);
        
        switch(result) {
        // The character is a dead key (accent or diacritic). In this case, it will alter
        // the keycode of the NEXT key, so we should ignore it.
        case -1:
            *valid = false;
            break;
        
        // If there is no unicode translation: arrow keys etc. Mark as special character and use virtual keycode.
        // Convert from unsigned to signed with a pointer cast.
        case 0:
            *valid = true;
            *special = true;
            *character = *((int *)(&virtualKeycode));
            break;
        
        // If one character was written, use it as the character output (not a special character).
        // Treat shift+tab as a special character, normal tab as a normal character. This way the UI system
        // can discern between forward and back tab.
        case 1:
            *valid = true;
            *special = (virtualKeycode == VK_TAB) ? (GetKeyState(VK_SHIFT) < 0) : false;
            *character = output;
            break;
        
        // In all other cases, ignore the character and print a warning.
        default:
            *valid = false;
            std::cerr << "WARNING: rascUIwin::Context: " << "ToUnicode reported that " << result << " characters were written to the buffer.\n";
            break;
        }
    }
}
