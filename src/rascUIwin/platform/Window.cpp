/*
 * Window.cpp
 *
 *  Created on: 18 May 2020
 *      Author: wilson
 */

#include "Window.h"

#include <rascUI/util/RepaintController.h>
#include <rascUI/platform/WindowConfig.h>
#include <windows.h>
#include <iostream>

#include "Context.h"

namespace rascUIwin {
    
    Window::Window(Context * context, const rascUI::WindowConfig & config, rascUI::Theme * theme, const rascUI::Location & location, const rascUI::Bindings * bindings, const GLfloat * xScalePtr, const GLfloat * yScalePtr) :
        rascUI::WindowBase((rascUI::ContextBase *)context, config, theme, location, bindings, xScalePtr, yScalePtr),
        
        // Generate a unique class name from the context's unique class number.
        windowClassName(std::string("rascUI-window-") + std::to_string(context->getUniqueClassNumber())),
        
        // Save whether double buffer is enabled, and initially use NULL for the off screen buffer.
        doubleBufferEnabled(config.doubleBufferEnabled),
        offScreenBuffer(NULL),
        
        lastMouseMoveX(0),
        lastMouseMoveY(0),
        isTrackingMouse(false) {
        
        // Set the TopLevelContainer before draw theme user data to a pointer to ourself:
        // this allows the Theme to be able to draw to us.
        beforeDrawThemeUserData = this;
        
        // For convenience.
        HINSTANCE hInstance = context->winGetHInstance();
        
        // Create the window class.
        windowClass.cbSize         = sizeof(WNDCLASSEX);                    // Size of structure.
        windowClass.style          = CS_HREDRAW | CS_VREDRAW;               // Tells the window to redraw when its size changes.
        windowClass.lpfnWndProc    = &Window::windowMessage;                // The function pointer to handle window messages.
        windowClass.cbClsExtra     = 0;                                     // Don't allocate extra class memory.
        windowClass.cbWndExtra     = 0;                                     // Don't allocate extra window memory.
        windowClass.hInstance      = hInstance;                             // The relevant hInstance.
        windowClass.hIcon          = LoadIcon(hInstance, IDI_APPLICATION);  // Predefined program icon.
        windowClass.hCursor        = LoadCursor(NULL, IDC_ARROW);           // Predefined cursor arror.
        windowClass.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);              // Use white brush for background (alternatively use "GetStockObject(WHITE_BRUSH);").
        windowClass.lpszMenuName   = NULL;                                  // Name of menu resource (don't use one).
        windowClass.lpszClassName  = windowClassName.c_str();               // The name of the window class.
        windowClass.hIconSm        = LoadIcon(hInstance, IDI_APPLICATION);  // The "small class icon" (alternatively use "LoadImage(hinstance, MAKEINTRESOURCE(5), IMAGE_ICON, GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR)").
        
        // Register the window class, complain (and set error) on failure.
        windowClassAtom = RegisterClassEx(&windowClass);
        if (windowClassAtom == 0) {
            std::cerr << "WARNING: rascUI::Window: " << "Failed to register window class, error code: " << GetLastError() << ".\n";
            context->notifyWindowError();
            windowHandle = 0;
            successfullyRegistered = false;
            return;
        }
        
        // Create the window (handle).
        windowHandle = CreateWindow(
            windowClassName.c_str(),                                      // The class name of the window.
            config.title.c_str(),                                         // What to put in the title bar.
            WS_OVERLAPPEDWINDOW,                                          // The type of window.
            config.enableScreenPosition ? config.screenX : CW_USEDEFAULT, // Window initial X position: Use either position from window config or default.
            config.enableScreenPosition ? config.screenY : CW_USEDEFAULT, // Window initial Y position: Use either position from window config or default.
            config.screenWidth, config.screenHeight,                      // Window width, height.
            NULL,                                                         // Parent window (none).
            NULL,                                                         // Menu bar: we are not using one.
            hInstance,                                                    // The related hInstance.
            NULL                                                          // Additional data (lpParam) - not needed here.
        );
        
        // Complain (and set error) on failure to create window.
        if (windowHandle == 0) {
            std::cerr << "WARNING: rascUI::Window: " << "Failed to create window handle, error code: " << GetLastError() << ".\n";
            context->notifyWindowError();
            successfullyRegistered = false;
            return;
        }
        
        // Register with context. Note that this only happens if we successfully created a window.
        successfullyRegistered = true;
        context->registerWindow(this);
    }
    
    Window::~Window(void) {
        // For convenience.
        Context * context = (Context *)getContext();
        
        // Unregister only if we successfully registered.
        if (successfullyRegistered) {
            context->unregisterWindow(this);
        }
        
        // Free the off screen buffer pixmap (if necessary).
        if (offScreenBuffer != NULL) {
            if (DeleteObject(offScreenBuffer) == 0) {
                std::cerr << "WARNING: rascUI::Window: " << "Failed to free off screen buffer: " << GetLastError() << ".\n";
            }
        }
        
        // If we created a window handle, destroy it and complain if doing so failed.
        if (windowHandle != 0) {
            if (DestroyWindow(windowHandle) == 0) {
                std::cerr << "WARNING: rascUI::Window: " << "Failed to destroy window handle, error code: " << GetLastError() << ".\n";
            }
        }
        
        // If we created a window class, unregister it and complain if doing so failed.
        if (windowClassAtom != 0) {
            if (UnregisterClass(windowClassName.c_str(), context->winGetHInstance()) == 0) {
                std::cerr << "WARNING: rascUI::Window: " << "Failed to unregister window class, error code: " << GetLastError() << ".\n";
            }
        }
    }
    
    LRESULT Window::windowMessage(HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam) {

        // Figure out which window the message relates to, using the global window map.
        Window * window = Context::winGetWindowFromHandle(windowHandle);
        
        // For windows we don't know about, run the default window procedure. This takes care of the
        // messages which windows are sent, BEFORE they are created. For some reason, if these are not
        // handled with the default window procedure, creating the window handle fails.
        if (window == NULL) {
            return DefWindowProc(windowHandle, message, wParam, lParam);
        
        // For windows that we *do* know about, handle the message 
        } else {
            return ((Context *)window->getContext())->runWindowMessage(window, windowHandle, message, wParam, lParam);
        }
    }
    
    void Window::onMapWindow(void) {
        // Showing the window should be done with the nCmdShow option, from our context.
        ShowWindow(windowHandle, ((Context *)getContext())->winGetNCmdShow());
    }
    
    void Window::onUnmapWindow(void) {
        // Strangely, hiding a window is also done with ShowWindow, but using the SW_HIDE mode parameter.
        //      See https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-showwindow
        //          http://web.archive.org/web/20190906223326/https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-showwindow
        // Also see https://stackoverflow.com/a/3016764
        //          http://web.archive.org/web/20170213185435/https://stackoverflow.com/questions/3016714/how-does-one-hide-a-win32-app-window
        ShowWindow(windowHandle, SW_HIDE);
    }
    
    void Window::winRepaintIfNeeded(void) {
        // Note that we must use GetClientRect rather than GetWindowRect, since getWindowRect INCLUDES the window borders.
        if (isMapped() && getRepaintController()->isRepaintRequired()) {
            RECT clientRect;
            if (GetClientRect(windowHandle, &clientRect) == 0) {
                std::cerr << "WARNING: rascUIwin::Window: " << "Failed to access window size, error code: " << GetLastError() << ".\n";
            } else {
                // Convert to a rascUI rectangle. Note that the clientRect's left and top might not be 0,0 - so use them.
                rascUI::Rectangle windowRect(clientRect.left, clientRect.top, clientRect.right - clientRect.left, clientRect.bottom - clientRect.top);
                doPartialRepaint(windowRect, 0.1f);
            }
        }
    }
}
